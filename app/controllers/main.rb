# -*- coding: utf-8 -*-
module SimpleBBS
  App.controllers do
    get "/" do
      @posts = Post.all.reverse
      slim :index
    end

    get "/article/:id" do |id|
      post = Post.select {|x| x.id == id.to_i}
      halt 404 unless post.any?
      post.first
    end

    post "/new" do
      po = Post.new
      po.title = params["title"]
      po.body = params["body"]
      begin
        po.save!
      rescue
        @message = "投稿に失敗しました。"
      end
      @page = nil
      @posts = Post.all.reverse
      slim :index
    end
  end
end

